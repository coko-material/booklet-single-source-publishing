class addId extends Paged.Handler {
    constructor(chunker, polisher, caller) {
      super(chunker, polisher, caller);
    }
  
    beforeParsed(content) {
        addIDtoEachElement(content);
    }
  }
  
  Paged.registerHandlers(addId);


function addIDtoEachElement(content) {
    let tags = ["figure", "figcaption", "img", "ol", "ul", "li", "p", "img", "table", "h1", "h2", "h3", "div", "aside"];
    tags.forEach( tag => {
      content.querySelectorAll(tag).forEach((el, index) => {
        if (!el.id) {
          el.id = `el-${el.tagName.toLowerCase()}-${index}`;
        }
      });
    })
  }


  